import React from "react";
import { Container, Row, Col, Spinner, Button } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import userAction from "../../redux/action/userAction";
import { useForm } from "react-hook-form";
import { Link, Navigate } from "react-router-dom";

import Cookies from "js-cookie";

function ForgotPass(Props) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (formData) => {
    debugger;
    const { getPassword } = Props.userAction;

    getPassword(formData.email);
  };

  const loadingComponent = (
    <div
      style={{
        position: "absolute",
        zIndex: 110,
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "rgba(255,255,255,0.8)",
      }}
    >
      <Spinner color="primary" className="reload-spinner" />
    </div>
  );

  return (
    <Container>
      {Props.loadingGetPassword ? <>{loadingComponent}</> : null}
      <Link to="/">
        <Button color="success">Home</Button>
      </Link>
      <Link to="/pages/login">
        <Button color="primary">Login page</Button>
      </Link>
      <h1>Forgot password</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col md="4">
            <label>Nhập email đăng ký: </label>
          </Col>
          <Col md="8">
            <input
              {...register("email", {
                required: "Email không được để trống",
                pattern: {
                  value:
                    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                  message: "Email không đúng định dạng",
                },
              })}
            />
            {errors.email && (
              <p style={{ color: "red" }}>{errors.email.message}</p>
            )}
          </Col>
          <Col md={12}>
            <button type="submit">Lấy mật khẩu</button>
          </Col>
        </Row>
      </form>
    </Container>
  );
}

const mapStateToProps = (state) => {
  return {
    loadingGetPassword: state.userReducer.loadingGetPassword,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    userAction: bindActionCreators(userAction, dispatch),
  };
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(ForgotPass);
