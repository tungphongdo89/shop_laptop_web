import React from "react";
import { Container, Row, Col, Spinner, Button } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import userAction from "../../redux/action/userAction";
import { useForm } from "react-hook-form";
import { Link, Navigate } from "react-router-dom";

import Cookies from "js-cookie";

function Login(Props) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (formData) => {
    debugger;
    const { login } = Props.userAction;

    let data = {};
    data.username = formData.username === "" ? null : formData.username;
    data.password = formData.password === "" ? null : formData.password;

    login(data);
  };

  const setAuth = (token, fullName, userId) => {
    debugger;
    let d = new Date();
    d.setTime(d.getTime() + 3600 * 1000);
    Cookies.set("token", token, { expires: d });
    Cookies.set("fullName", fullName, { expires: d });
    Cookies.set("userId", userId, { expires: d });
  };

  const loadingComponent = (
    <div
      style={{
        position: "absolute",
        zIndex: 110,
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "rgba(255,255,255,0.8)",
      }}
    >
      <Spinner color="primary" className="reload-spinner" />
    </div>
  );

  return (
    <Container>
      {Props.loadingLogin ? <>{loadingComponent}</> : null}
      <Link to="/">
        <Button color="success">Home</Button>
      </Link>
      <h1>LOGIN PAGE</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col md="4">
            <label>Username</label>
          </Col>
          <Col md="8">
            <input
              {...register("username", {
                required: "Tên tài khoản không được để trống",
                minLength: {
                  value: 1,
                  message: "Tên tài khoản phải có ít nhất 1 ký tự",
                },
                maxLength: {
                  value: 50,
                  message: "Tên tài khoản không được quá 50 ký tự",
                },
              })}
            />
            {errors.username && (
              <p style={{ color: "red" }}>{errors.username.message}</p>
            )}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <label>Password</label>
          </Col>
          <Col md="8">
            <input
              type="password"
              {...register("password", {
                required: "Mật khẩu không được để trống",
                minLength: {
                  value: 1,
                  message: "Mật khẩu phải có ít nhất 1 ký tự",
                },
                maxLength: {
                  value: 50,
                  message: "Mật khẩu không được quá 50 ký tự",
                },
              })}
            />
            {errors.password && (
              <p style={{ color: "red" }}>{errors.password.message}</p>
            )}
          </Col>
          <Col md={12}>
            <button type="submit">Đăng nhập</button>
          </Col>
        </Row>
      </form>

      <Row>
        <Col md={12}>
          <Link to="/pages/create-account">Create account</Link>
        </Col>
        <Col md={12}>
          <Link to="/pages/forgot-pass">Forgot password</Link>
        </Col>
      </Row>

      {Props.loginSuccess ? (
        <>
          {setAuth(Props.token, Props.fullName, Props.userId)}
          <Navigate to="/" />
        </>
      ) : null}
    </Container>
  );
}

const mapStateToProps = (state) => {
  return {
    statusLogin: state.userReducer.statusLogin,
    loadingLogin: state.userReducer.loadingLogin,
    loginSuccess: state.userReducer.loginSuccess,
    token: state.userReducer.token,
    fullName: state.userReducer.fullName,
    userId: state.userReducer.userId,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    userAction: bindActionCreators(userAction, dispatch),
  };
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Login);
