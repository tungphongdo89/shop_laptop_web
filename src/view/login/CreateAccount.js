import React, { useState } from "react";
import { Container, Row, Col, Spinner, Button } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import userAction from "../../redux/action/userAction";
import { useForm } from "react-hook-form";
import { Link, Navigate } from "react-router-dom";

import Cookies from "js-cookie";

function Login(Props) {
  const [gender, setGender] = useState("");
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onChangeGender = (gender) => {
    setGender(gender);
  };

  const onSubmit = (formData) => {
    debugger;
    const { createAccount } = Props.userAction;
    let userInfo = { ...formData };
    userInfo.age = parseInt(userInfo.age);
    userInfo.gender = parseInt(userInfo.gender);
    userInfo.userRoleName = "ROLE_USER";
    createAccount(userInfo);
  };

  const loadingComponent = (
    <div
      style={{
        position: "absolute",
        zIndex: 110,
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "rgba(255,255,255,0.8)",
      }}
    >
      <Spinner color="primary" className="reload-spinner" />
    </div>
  );

  return (
    <Container>
      {Props.loadingCreateAccount ? <>{loadingComponent}</> : null}
      <>
        <Link to="/">
          <Button color="success">Home</Button>
        </Link>
        <Link to="/pages/login">
          <Button color="primary">Login page</Button>
        </Link>
      </>
      <h1>Create account</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col md="4">
            <label>Username</label>
          </Col>
          <Col md="8">
            <input
              {...register("username", {
                required: "Tên tài khoản không được để trống",
                minLength: {
                  value: 1,
                  message: "Tên tài khoản phải có ít nhất 1 ký tự",
                },
                maxLength: {
                  value: 50,
                  message: "Tên tài khoản không được quá 50 ký tự",
                },
              })}
            />
            {errors.username && (
              <p style={{ color: "red" }}>{errors.username.message}</p>
            )}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <label>Password</label>
          </Col>
          <Col md="8">
            <input
              type="password"
              {...register("password", {
                required: "Mật khẩu không được để trống",
                minLength: {
                  value: 1,
                  message: "Mật khẩu phải có ít nhất 1 ký tự",
                },
                maxLength: {
                  value: 50,
                  message: "Mật khẩu không được quá 50 ký tự",
                },
              })}
            />
            {errors.password && (
              <p style={{ color: "red" }}>{errors.password.message}</p>
            )}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <label>Full name</label>
          </Col>
          <Col md="8">
            <input
              {...register("fullName", {
                required: "Họ và tên không được để trống",
                minLength: {
                  value: 1,
                  message: "Họ và tên phải có ít nhất 1 ký tự",
                },
                maxLength: {
                  value: 100,
                  message: "Họ và tên không được quá 100 ký tự",
                },
              })}
            />
            {errors.fullName && (
              <p style={{ color: "red" }}>{errors.fullName.message}</p>
            )}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <label>Age</label>
          </Col>
          <Col md="8">
            <input
              type="number"
              {...register("age", {
                required: "Tuổi không được để trống",
                min: {
                  value: 1,
                  message: "Tuổi phải lớn hơn hoặc bằng 1",
                },
                max: {
                  value: 9999,
                  message: "Tuổi phải bé hơn hoặc bằng 9999",
                },
              })}
            />
            {errors.age && <p style={{ color: "red" }}>{errors.age.message}</p>}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <label>Gender</label>
          </Col>
          <Col md="8">
            <select {...register("gender")} onChange={(e) => onChangeGender(e)}>
              <option value={0}>Nam</option>
              <option value={1}>Nữ</option>
              <option value={2}>Khác</option>
            </select>
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <label>Phone number</label>
          </Col>
          <Col md="8">
            <input
              {...register("phoneNumber", {
                required: "Số điện thoại không được để trống",
                pattern: {
                  value: "^$|[0-9]{10}",
                  message: "Số điện thoại không đúng định dạng",
                },
              })}
            />
            {errors.phoneNumber && (
              <p style={{ color: "red" }}>{errors.phoneNumber.message}</p>
            )}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <label>Email</label>
          </Col>
          <Col md="8">
            <input
              {...register("email", {
                required: "Email không được để trống",
                pattern: {
                  value:
                    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                  message: "Email không đúng định dạng",
                },
              })}
            />
            {errors.email && (
              <p style={{ color: "red" }}>{errors.email.message}</p>
            )}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <label>Address</label>
          </Col>
          <Col md="8">
            <input
              {...register("address", {
                required: "Địa chỉ không được để trống",
                minLength: {
                  value: 1,
                  message: "Địa chỉ phải có ít nhất 1 ký tự",
                },
                maxLength: {
                  value: 200,
                  message: "Địa chỉ không được quá 200 ký tự",
                },
              })}
            />
            {errors.address && (
              <p style={{ color: "red" }}>{errors.address.message}</p>
            )}
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <button type="submit">Đăng ký</button>
          </Col>
        </Row>
      </form>

      {/* {Props.loginSuccess ? (
        <>
          {setAuth(Props.token, Props.fullName, Props.userId)}
          <Navigate to="/" />
        </>
      ) : null} */}
    </Container>
  );
}

const mapStateToProps = (state) => {
  return {
    loadingCreateAccount: state.userReducer.loadingCreateAccount,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    userAction: bindActionCreators(userAction, dispatch),
  };
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Login);
