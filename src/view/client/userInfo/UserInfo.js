import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import userAction from "../../../redux/action/userAction";
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import Navbar from "../Navbar";
import { Navigate } from "react-router";
import ModalChangePass from "./ModalChangePass";

function UserInfo(Props) {
  const [openModalChangePass, setOpenModalChangePass] = useState(false);

  useEffect(() => {
    debugger;
    getUserDetail(Cookies.get("userId"));
  }, []);

  const getUserDetail = (userId) => {
    debugger;
    const { getUserDetail } = Props.userAction;

    getUserDetail(userId);
  };

  const handleOpenModalChangePass = (hideOrShow) => {
    debugger;
    setOpenModalChangePass(hideOrShow);
  };

  return (
    <Container>
      <Navbar />
      <h1>USER INFORMATION PAGE</h1>

      {Props.userInformation ? (
        <>
          <h2>Infor user</h2>
          <Row>
            <Col md="4">
              <p>userCode: </p>
            </Col>
            <Col md="8">{Props.userInformation.userCode}</Col>
          </Row>

          <Row>
            <Col md="4">
              <p>username: </p>
            </Col>
            <Col md="8">{Props.userInformation.username}</Col>
          </Row>

          <Row>
            <Col md="4">
              <p>fullName: </p>
            </Col>
            <Col md="8">{Props.userInformation.fullName}</Col>
          </Row>

          <Row>
            <Col md="4">
              <p>age: </p>
            </Col>
            <Col md="8">{Props.userInformation.age}</Col>
          </Row>

          <Row>
            <Col md="4">
              <p>gender: </p>
            </Col>
            <Col md="8">{Props.userInformation.gender}</Col>
          </Row>

          <Row>
            <Col md="4">
              <p>phoneNumber: </p>
            </Col>
            <Col md="8">{Props.userInformation.phoneNumber}</Col>
          </Row>

          <Row>
            <Col md="4">
              <p>email: </p>
            </Col>
            <Col md="8">{Props.userInformation.email}</Col>
          </Row>

          <Row>
            <Col md="4">
              <p>address: </p>
            </Col>
            <Col md="8">{Props.userInformation.address}</Col>
          </Row>
        </>
      ) : null}

      {openModalChangePass ? (
        <ModalChangePass
          handleOpenModalChangePass={handleOpenModalChangePass}
          openModalChangePass={openModalChangePass}
        />
      ) : null}

      <Row>
        <Col md={6}>
          <Button color="success">Update information</Button>
        </Col>
        <Col md={6}>
          <Button onClick={() => handleOpenModalChangePass(true)}>
            Change passwod
          </Button>
        </Col>
      </Row>

      {Cookies.get("token") ? null : (
        <>
          <Navigate to="/" />
        </>
      )}
    </Container>
  );
}

const mapStateToProps = (state) => {
  return {
    userInformation: state.userReducer.userInformation,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    userAction: bindActionCreators(userAction, dispatch),
  };
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(UserInfo);
