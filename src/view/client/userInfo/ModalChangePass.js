import React, { useState } from "react";
import {
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Spinner,
} from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import userAction from "../../../redux/action/userAction";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Cookies from "js-cookie";

const schema = Yup.object()
  .shape({
    // age: Yup.number()
    //   .required("tuổi không được để trống")
    //   .moreThan(0, "tuổi phải lón hơn 0")
    //   .lessThan(20, "tuổi phải bé hơn 20"),
    currentPassword: Yup.string().required(
      "Mật khẩu hiện tại không được bỏ trống"
    ),
    newPassword: Yup.string()
      .required("Mật khẩu mới không được bỏ trống")
      .min(8, "Mật khẩu mới phải có ít nhất 8 ký tự"),
    ConfirmNewPassword: Yup.string()
      .required("Nhập lại mật khẩu không được bỏ trống")
      .oneOf([Yup.ref("newPassword")], "Mật khẩu không khớp"),
  })
  .required();

function ModalChangePass(Props) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const handleCloseModal = () => {
    Props.handleOpenModalChangePass(false);
  };

  const onSubmit = (formData) => {
    debugger;
    console.log(formData);

    const { changePass } = Props.userAction;

    let changePassObj = {};
    changePassObj.id = parseInt(Cookies.get("userId"));
    changePassObj.currentPass = formData.currentPassword;
    changePassObj.newPass = formData.newPassword;
    changePass(changePassObj);
  };

  const loadingComponent = (
    <div
      style={{
        position: "absolute",
        zIndex: 110,
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "rgba(255,255,255,0.8)",
      }}
    >
      <Spinner color="primary" className="reload-spinner" />
    </div>
  );

  return (
    <Modal isOpen={Props.openModalChangePass}>
      {Props.loadingChangePass ? <>{loadingComponent}</> : null}
      <ModalHeader>Đổi mật khẩu</ModalHeader>
      <ModalBody>
        <form onSubmit={handleSubmit(onSubmit)}>
          {/* <Row>
            <Col md="4">
              <label>tuổi</label>
            </Col>
            <Col md="8">
              <input type="number" {...register("age")} />
              {errors.age && (
                <p style={{ color: "red" }}>{errors.age.message}</p>
              )}
            </Col>
          </Row> */}
          <Row>
            <Col md="4">
              <label>Current password</label>
            </Col>
            <Col md="8">
              <input type="password" {...register("currentPassword")} />
              {errors.currentPassword && (
                <p style={{ color: "red" }}>{errors.currentPassword.message}</p>
              )}
            </Col>
          </Row>
          <Row>
            <Col md="4">
              <label>New password</label>
            </Col>
            <Col md="8">
              <input type="password" {...register("newPassword")} />
              {errors.newPassword && (
                <p style={{ color: "red" }}>{errors.newPassword.message}</p>
              )}
            </Col>
          </Row>
          <Row>
            <Col md="4">
              <label>Confirm password</label>
            </Col>
            <Col md="8">
              <input type="password" {...register("ConfirmNewPassword")} />
              {errors.ConfirmNewPassword && (
                <p style={{ color: "red" }}>
                  {errors.ConfirmNewPassword.message}
                </p>
              )}
            </Col>
          </Row>
          <button color="success" type="submit">
            change
          </button>
        </form>
      </ModalBody>
      <ModalFooter>
        <Button color="danger" onClick={handleCloseModal}>
          close
        </Button>
        <button color="success" type="submit">
          change
        </button>
      </ModalFooter>
    </Modal>
  );
}

const mapStateToProps = (state) => {
  return {
    loadingChangePass: state.userReducer.loadingChangePass,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    userAction: bindActionCreators(userAction, dispatch),
  };
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(ModalChangePass);
