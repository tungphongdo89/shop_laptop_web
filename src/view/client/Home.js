import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { Container, Button } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import userAction from "../../redux/action/userAction";
import Navbar from "./Navbar";

function Home(Props) {
  return (
    <Container>
      <h1>HOME PAGE</h1>
      <Navbar />
    </Container>
  );
}

const mapStateToProps = (state) => {
  return {};
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    userAction: bindActionCreators(userAction, dispatch),
  };
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Home);
