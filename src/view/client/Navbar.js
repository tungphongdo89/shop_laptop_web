import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { Container, Button } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import userAction from "../../redux/action/userAction";
import Cookies from "js-cookie";

function Navbar(Props) {
  useEffect(() => {
    debugger;
    console.log(typeof Cookies.get("token"));
    if (Cookies.get("token") && Props.loginSuccess === false) {
      const { setLoginSuccess } = Props.userAction;
      setLoginSuccess();
    }
  });

  const logout = () => {
    debugger;
    const { logout } = Props.userAction;
    logout();
  };

  return (
    <Container>
      {Cookies.get("token") ? (
        <>
          <Link to="/">
            <Button color="success">Home</Button>
          </Link>
          <>{"  "}</>
          <Link to="/pages/user-information">
            <Button color="success">{Cookies.get("fullName")}</Button>
          </Link>
          <>{"  "}</>
          <Button color="primary" onClick={logout}>
            Logout
          </Button>
        </>
      ) : (
        <>
          <Link to="/">
            <Button color="success">Home</Button>
          </Link>
          <>{"  "}</>
          <Link to="/pages/login">
            <Button color="primary">Login page</Button>
          </Link>
        </>
      )}
    </Container>
  );
}

const mapStateToProps = (state) => {
  return {
    loginSuccess: state.userReducer.loginSuccess,
  };
};
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    userAction: bindActionCreators(userAction, dispatch),
  };
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Navbar);
