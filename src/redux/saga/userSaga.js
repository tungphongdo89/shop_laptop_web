import { call, put, takeEvery } from "redux-saga/effects";
import * as config from "../common/const";
import userAction from "../action/userAction";
import userApi from "../api/userApi";

function* login({ payload }) {
  debugger;
  const data = payload;
  const resp = yield call(userApi.login, data);
  if (200 === resp.statusCode) {
    yield put(userAction.loginSuccess(resp));
  } else {
    yield put(userAction.loginFailed(resp));
  }
}

function* getPassword({ payload }) {
  debugger;
  const data = payload;
  const resp = yield call(userApi.getPassword, data);
  if (200 === resp.statusCode) {
    yield put(userAction.getPasswordSuccess(resp));
  } else {
    yield put(userAction.getPasswordFailed(resp));
  }
}

function* changePass({ payload }) {
  debugger;
  const data = payload;
  const resp = yield call(userApi.changePass, data);
  if (200 === resp.statusCode) {
    yield put(userAction.changePassSuccess(resp));
  } else {
    yield put(userAction.changePassFailed(resp));
  }
}

function* getUserDetail({ payload }) {
  debugger;
  const data = payload;
  const resp = yield call(userApi.getUserDetail, data);
  if (200 === resp.statusCode) {
    yield put(userAction.getUserDetailSuccess(resp));
  } else {
    yield put(userAction.getUserDetailFailed(resp));
  }
}

function* createAccount({ payload }) {
  debugger;
  const data = payload;
  const resp = yield call(userApi.createAccount, data);
  if (200 === resp.statusCode) {
    yield put(userAction.createAccountSuccess(resp));
  } else {
    yield put(userAction.createAccountFailed(resp));
  }
}

function* userSaga() {
  yield takeEvery(config.LOGIN, login);
  yield takeEvery(config.GET_PASSWORD, getPassword);
  yield takeEvery(config.CHANGE_PASS, changePass);
  yield takeEvery(config.GET_USER_DETAIL, getUserDetail);
  yield takeEvery(config.CREATE_ACCOUNT, createAccount);
}

export default userSaga;
