import * as config from "../common/const";

const userAction = {
  login,
  loginSuccess,
  loginFailed,

  logout,
  setLoginSuccess,

  getPassword,
  getPasswordSuccess,
  getPasswordFailed,

  changePass,
  changePassSuccess,
  changePassFailed,

  getUserDetail,
  getUserDetailSuccess,
  getUserDetailFailed,

  createAccount,
  createAccountSuccess,
  createAccountFailed,
};

// login
function login(data) {
  debugger;
  return {
    type: config.LOGIN,
    payload: data,
  };
}

function loginSuccess(data) {
  debugger;
  return {
    type: config.LOGIN_SUCCESS,
    payload: data,
  };
}

function loginFailed(data) {
  debugger;
  return {
    type: config.LOGIN_FAILED,
    payload: data,
  };
}

// Get password
function getPassword(data) {
  debugger;
  return {
    type: config.GET_PASSWORD,
    payload: data,
  };
}

function getPasswordSuccess(data) {
  debugger;
  return {
    type: config.GET_PASS_SUCCESS,
    payload: data,
  };
}

function getPasswordFailed(data) {
  debugger;
  return {
    type: config.GET_PASS_FAILED,
    payload: data,
  };
}

// logout
function logout(data) {
  debugger;
  return {
    type: config.LOGOUT,
    payload: data,
  };
}

// set login allways success when token is not empty
function setLoginSuccess() {
  debugger;
  return {
    type: config.SET_LOGIN_SUCCESS,
  };
}

//Get user detail
function getUserDetail(data) {
  debugger;
  return {
    type: config.GET_USER_DETAIL,
    payload: data,
  };
}

function getUserDetailSuccess(data) {
  debugger;
  return {
    type: config.GET_USER_DETAIL_SUCCESS,
    payload: data,
  };
}

function getUserDetailFailed(data) {
  debugger;
  return {
    type: config.GET_USER_DETAIL_FAILED,
    payload: data,
  };
}

// change password
function changePass(data) {
  debugger;
  return {
    type: config.CHANGE_PASS,
    payload: data,
  };
}

function changePassSuccess(data) {
  debugger;
  return {
    type: config.CHANGE_PASS_SUCCESS,
    payload: data,
  };
}

function changePassFailed(data) {
  debugger;
  return {
    type: config.CHANGE_PASS_FAILED,
    payload: data,
  };
}

//Create account
function createAccount(data) {
  debugger;
  return {
    type: config.CREATE_ACCOUNT,
    payload: data,
  };
}

function createAccountSuccess(data) {
  debugger;
  return {
    type: config.CREATE_ACCOUNT_SUCCESS,
    payload: data,
  };
}

function createAccountFailed(data) {
  debugger;
  return {
    type: config.CREATE_ACCOUNT_FAILED,
    payload: data,
  };
}

export default userAction;
