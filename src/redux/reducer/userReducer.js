import * as config from "../common/const";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Cookies from "js-cookie";

const initState = {
  loadingLogin: false,
  token: "",
  fullName: "",
  userId: 0,
  loginSuccess: false,
  loadingLogout: false,
  userInformation: {},
  loadingCreateAccount: false,
  loadingGetPassword: false,
  loadingChangePass: false,
};

const userReducer = (state = initState, action) => {
  switch (action.type) {
    //login
    case config.LOGIN: {
      debugger;
      state = { ...state, loadingLogin: true };
      return state;
    }
    case config.LOGIN_SUCCESS: {
      debugger;
      const data = action.payload;
      state = {
        ...state,
        loadingLogin: false,
        loginSuccess: true,
        token: data.token,
        fullName: data.fullName,
        userId: data.userId,
      };
      toast.success(data.message);
      return state;
    }
    case config.LOGIN_FAILED: {
      debugger;
      const data = action.payload;
      state = { ...state, loadingLogin: false };
      toast.error(data.message);
      return state;
    }

    //logout
    case config.LOGOUT: {
      debugger;
      Cookies.remove("token");
      Cookies.remove("fullName");
      Cookies.remove("userId");
      state = {
        ...state,
        token: "",
        fullName: "",
        userId: 0,
        loginSuccess: false,
        userInformation: {},
      };
      toast.success("logout success");
      return state;
    }

    //get password
    case config.GET_PASSWORD: {
      debugger;
      state = { ...state, loadingGetPassword: true };
      return state;
    }
    case config.GET_PASS_SUCCESS: {
      debugger;
      const data = action.payload;
      state = { ...state, loadingGetPassword: false };
      toast.success(data.message);
      return state;
    }
    case config.GET_PASS_FAILED: {
      debugger;
      const data = action.payload;
      state = { ...state, loadingGetPassword: false };
      toast.error(data.message);
      return state;
    }

    //set login allways success when token is not empty
    case config.SET_LOGIN_SUCCESS: {
      debugger;
      state = { ...state, loginSuccess: true };
      return state;
    }

    //change pass
    case config.CHANGE_PASS: {
      debugger;
      state = { ...state, loadingChangePass: true };
      return state;
    }
    case config.CHANGE_PASS_SUCCESS: {
      debugger;
      const data = action.payload;
      state = {
        ...state,
        loadingChangePass: false,
      };
      toast.success(data.message);
      return state;
    }
    case config.CHANGE_PASS_FAILED: {
      debugger;
      const data = action.payload;
      state = { ...state, loadingChangePass: false };
      toast.error(data.message);
      return state;
    }

    //get user detail
    case config.GET_USER_DETAIL: {
      debugger;
      state = { ...state };
      return state;
    }
    case config.GET_USER_DETAIL_SUCCESS: {
      debugger;
      const data = action.payload.data;
      state = { ...state, userInformation: data };
      toast.success(data.message);
      return state;
    }
    case config.GET_USER_DETAIL_FAILED: {
      debugger;
      const data = action.payload.data;
      toast.error(data.message);
      return state;
    }

    //create account
    case config.CREATE_ACCOUNT: {
      debugger;
      state = { ...state, loadingCreateAccount: true };
      return state;
    }
    case config.CREATE_ACCOUNT_SUCCESS: {
      debugger;
      const data = action.payload;
      state = { ...state, loadingCreateAccount: false };
      toast.success(data.message);
      return state;
    }
    case config.CREATE_ACCOUNT_FAILED: {
      debugger;
      const data = action.payload;
      state = { ...state, loadingCreateAccount: false };
      toast.error(data.message);
      return state;
    }

    default:
      return state;
  }
};

export default userReducer;
