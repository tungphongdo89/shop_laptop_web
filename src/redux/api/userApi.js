import Cookies from "js-cookie";

const userApi = {
  login,
  getPassword,
  changePass,
  getUserDetail,
  createAccount,
};

async function login(data) {
  debugger;
  return await fetch("/api/user/login", {
    method: "POST",
    headers: {
      "Accept-Language": "us",
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((body) => body)
    .catch((error) => {
      throw error;
    });
}

async function getPassword(data) {
  debugger;
  return await fetch("/api/user/forgot-pass?email=" + data + "", {
    method: "GET",
    headers: {
      "Accept-Language": "us",
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((body) => body)
    .catch((error) => {
      throw error;
    });
}

async function changePass(data) {
  debugger;
  return await fetch("/api/user/change-pass", {
    method: "PATCH",
    headers: {
      Authorization: Cookies.get("token"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((body) => body)
    .catch((error) => {
      throw error;
    });
}

async function getUserDetail(data) {
  debugger;
  return await fetch("/api/user/detail?id=" + data + "", {
    method: "GET",
    headers: {
      Authorization: Cookies.get("token"),
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((body) => body)
    .catch((error) => {
      throw error;
    });
}

async function createAccount(data) {
  debugger;
  return await fetch("/api/user/create-user", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((body) => body)
    .catch((error) => {
      throw error;
    });
}

export default userApi;
