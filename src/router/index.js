import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import history from "./history";

import Login from "../view/login/index";
import Home from "../view/client/Home";
import UserInfo from "../view/client/userInfo/UserInfo";
import CreateAccount from "../view/login/CreateAccount";
import ForgotPass from "../view/login/ForgotPass";

function RootRouter() {
  return (
    <BrowserRouter history={history}>
      <Routes>
        <Route path="/pages/login" element={<Login />} exact />
        <Route path="/" element={<Home />} exact />
        <Route path="/pages/user-information" element={<UserInfo />} exact />
        <Route path="/pages/create-account" element={<CreateAccount />} exact />
        <Route path="/pages/forgot-pass" element={<ForgotPass />} exact />
      </Routes>
    </BrowserRouter>
  );
}

export default RootRouter;
