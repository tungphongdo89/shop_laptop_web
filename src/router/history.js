import { createBrowserHistory } from "history";

const history = createBrowserHistory({baseName: "/"});

export  default history;