# syntax=docker/dockerfile:1

FROM node:15.13-alpine

WORKDIR /app

ENV NODE_ENV="./node_modules/.bin:$PATH"

COPY . .

RUN npm run build

CMD [ "npm", "start" ]